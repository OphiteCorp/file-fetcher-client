# File Fetcher Client

PHP client for Fetch File application (server). This client presents a gallery of photos and images.

## Requirements
- Web server with PHP support (for example Apache, WAMP, EasyPHP)
- File Fetcher server (ideally located on the same server as the Web server)
- Optimized for browsers: FireFox, Chrome, Brave. Edge can also be used in an emergency. IE is not supported!

#### PHP
- Set the correct timezone on the server - [List of Supported Timezones](https://www.php.net/manual/en/timezones.php)
    - A) php.ini file `date.timezone` or
    - B) insert at the beginning of index.php `date_default_timezone_set('.../...");`
- Enable openssl extension in PHP
- Use PHP version 7.4 and later

## Properties

- [x] Optimized for many photos (fast loading)
- [x] Category support and an unlimited number of nested categories.
- [x] Image support: jpg, png, gif
- [x] Video support: mp4
- [x] AJAX thumbnail loading (asynchronous)
- [x] Thumbnail cache support (for later quick viewing)
- [x] Responsive appearance
- [x] Support for access to private categories using a username and password
- [x] Modern look
- [x] Localization support
- [x] High security. The password and tokens work over AES encryption with SHA3-256 support

## Configuration

The configuration is located in a file: `settings.php`

## Server

Server (service) version `1.2.0` is required
