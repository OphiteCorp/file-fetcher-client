<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/settings.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/src/api/server-api-wrapper-resolver.php';

/**
 * Hlavní třída s logikou pro galerii.
 */
final class Gallery {

    public const
        PARAM_FILE_GUID       = FileFetcherApiWrapperResolver::PARAM_FILE_GUID,
        PARAM_FILE_CHECKSUM   = FileFetcherApiWrapperResolver::PARAM_FILE_CHECKSUM,
        PARAM_CATEGORY_GUID   = FileFetcherApiWrapperResolver::PARAM_CATEGORY_GUID,
        PARAM_PREVIEW_MODE    = FileFetcherApiWrapperResolver::PARAM_PREVIEW_MODE,
        PARAM_IMAGE_WIDTH     = FileFetcherApiWrapperResolver::PARAM_IMAGE_WIDTH,
        PARAM_IMAGE_HEIGHT    = FileFetcherApiWrapperResolver::PARAM_IMAGE_HEIGHT,
        PARAM_TOKEN           = TokenProvider::PARAM_TOKEN,
        // vlastní parametry
        PARAM_RANDOM          = 'random'; // parametr v URL, který definuje zobrazení pouze náhodných souborů

    private
        $startTime,
        $apiResolver,
        $localizer,
        $tokenData,
        $catData,
        $statsData,
        $rootData;

    public function __construct() {
        $this->startTime = microtime(true);
        $this->apiResolver = new FileFetcherApiWrapperResolver();
        $this->localizer = Localizer::instance();
        $this->tokenData = $this->apiResolver->getToken();
        $this->rootData = $this->apiResolver->execute(FileFetcherApiWrapperResolver::TYPE_ROOT);
        $this->statsData = $this->apiResolver->execute(FileFetcherApiWrapperResolver::TYPE_STATS);
        $this->catData = $this->loadCatData();
    }

    public function login(?string $login, ?string $password) : void {
        $this->tokenData = $this->apiResolver->login($login, $password);
    }

    public function changePassword(?string $login, ?string $password, ?string $newPassword) : bool {
        $result = $this->apiResolver->changePassword($login, $password, $newPassword);

        if ($result->changed) {
            $this->tokenData = $result->token;
        }
        return $result->changed;
    }

    public function getCatData() : CategoryResponseDto {
        return $this->catData;
    }

    public function getStatsData() : StatsResponseDto {
        return $this->statsData;
    }

    public function getTokenData() : object {
        return $this->tokenData;
    }

    public function getFilePreviewUrl(?string $guid, ?int $checksum, ?int $width, ?int $height, ?int $previewMode = FileFetcherApiWrapperResolver::PM_THUMB) : string {
        $params = [];

        if ($guid != null) {
            $params[self::PARAM_FILE_GUID] = $guid;
        }
        if ($checksum != null) {
            $params[self::PARAM_FILE_CHECKSUM] = $checksum;
        }
        if ($width != null && $width > 0) {
            $params[self::PARAM_IMAGE_WIDTH] = $width;
        }
        if ($height != null && $height > 0) {
            $params[self::PARAM_IMAGE_HEIGHT] = $height;
        }
        if ($this->tokenData->nextToken != null) {
            $params[self::PARAM_TOKEN] = $this->tokenData->nextToken;
        }
        if ($previewMode != null) {
            $params[self::PARAM_PREVIEW_MODE] = $previewMode;
        }
        $query = http_build_query($params);
        return 'pub/preview.php?'.$query;
    }

    public function getFileOpenUrl($guid, $checksum) : string {
        $params = [self::PARAM_FILE_GUID => $guid, self::PARAM_FILE_CHECKSUM => $checksum];

        if ($this->tokenData->nextToken != null) {
            $params[self::PARAM_TOKEN] = $this->tokenData->nextToken;
        }
        $query = http_build_query($params);
        return 'pub/open.php?'.$query;
    }

    public function getFileDownloadUrl($guid) : string {
        $params = [self::PARAM_FILE_GUID => $guid];

        if ($this->tokenData->nextToken != null) {
            $params[self::PARAM_TOKEN] = $this->tokenData->nextToken;
        }
        $query = http_build_query($params);
        return 'pub/download.php?'.$query;
    }

    public function loadHeaderData() : object {
        $categoryGuid = FileFetcherApiWrapperResolver::readUrlParam(self::PARAM_CATEGORY_GUID);
        $output = (object) [
            'categoryGuid'  => $this->catData->isEmpty() ? null : $categoryGuid,
            'title'         => $this->localizer->tr(LC::PAGE_TITLE),
            'description'   => $this->localizer->tr(LC::PAGE_DESC),
            'author'        => $this->localizer->tr(LC::PAGE_AUTHOR),
            'keywords'      => $this->localizer->tr(LC::PAGE_KEYWORDS),
            'ogTitle'       => $this->localizer->tr(LC::PAGE_TITLE),
            'ogDescription' => $this->localizer->tr(LC::PAGE_DESC),
            'ogImageUrl'    => 'static/img/category/3.png'
        ];
        // parametr kategorie bude v URL existovat
        if (!empty($output->categoryGuid)) {
            $pathsCount = count($this->catData->paths);
            $ogTitle = '';

            for ($i = 1; $i < $pathsCount; $i++) {
                $path = $this->catData->paths[$i];
                $ogTitle .= $path->name;

                if ($i < $pathsCount - 1) {
                    $ogTitle .= ' > ';
                } else {
                    if (isset($path->description)) {
                        $output->ogDescription = $path->description;
                    }
                }
            }
            if (strlen($ogTitle) > 0) {
                $output->ogTitle = $ogTitle;
            }
        }
        // náhodný soubor jako náhled do kategorie
        if (!empty($this->catData->files)) {
            $file = $this->catData->files[array_rand($this->catData->files)];
            $output->ogImageUrl = $this->getFilePreviewUrl($file->guid, $file->checksum, $file->imgWidth, $file->imgHeight);
        }
        return $output;
    }

    public function getUrlQueryParams(array $ignoreParams = []) : string {
        $uri = $_SERVER['REQUEST_URI'];
        $parsed = parse_url($uri);
        $query = @$parsed['query'];

        // v URL neexistují žádné parametry
        if ($query == null) {
            return '';
        }
        parse_str($query, $params);
        $newQuery = '';

        foreach ($params as $param => $value) {
            if (in_array($param, $ignoreParams)) {
                continue;
            }
            // pokud bude v parametrech token a bude platný, tak ho aktualizuje, jinak odebere
            if ($param == self::PARAM_TOKEN && $this->tokenData->nextToken != null) {
                $value = $this->tokenData->nextToken;
            } else {
                continue;
            }
            if (strlen($value) > 0) {
                $newQuery .= $param.'='.$value;
            } else {
                $newQuery .= $param;
            }
            // za každým parametrem se přidá '&'
            if (!empty($newQuery)) {
                $newQuery .= '&';
            }
        }
        // odebere poslední '&'
        if (!empty($newQuery)) {
            $newQuery = substr($newQuery, 0, -1);
        }
        return $newQuery;
    }

    public function getYearRange() : string {
        $year = date('Y');
        return ($year > DEF_YEAR_OF_CREATION) ? DEF_YEAR_OF_CREATION.' - '.$year : DEF_YEAR_OF_CREATION;
    }

    public function isServiceAvailable() : bool {
        return empty(!$this->rootData->version);
    }

    public function getLocalizer() : Localizer {
        return $this->localizer;
    }

    public function isEmpty() : bool {
        return $this->getCatData()->isEmpty();
    }

    public function calculateLoadTime() : float {
        return round(microtime(true) - $this->startTime, 6);
    }

    public function generateGuid() : string {
        return Utils::generateGuid();
    }

    public function getServerVersion() : string {
        return $this->isServiceAvailable() ? 'v'.$this->rootData->version : $this->localizer->tr(LC::OFFLINE);
    }

    private function loadCatData() : CategoryResponseDto {
        $catData = CategoryResponseDto::mapFromResponse(null);

        if (isset($_GET[self::PARAM_RANDOM])) {
            $paramValue = $_GET[self::PARAM_RANDOM];
            $categoryValue = (isset($_GET[self::PARAM_CATEGORY_GUID]) && !empty($_GET[self::PARAM_CATEGORY_GUID])) ? $_GET[self::PARAM_CATEGORY_GUID] : null;
            $catData->files = $this->apiResolver->execute(FileFetcherApiWrapperResolver::TYPE_RANDOM_FILE_INFO, [
                'categoryGuid' => $categoryValue,
                'count'        => !empty($paramValue) ? $paramValue : 1
            ]);
        } else {
            $catData = $this->apiResolver->execute(FileFetcherApiWrapperResolver::TYPE_CAT);
        }
        return $catData;
    }
}
