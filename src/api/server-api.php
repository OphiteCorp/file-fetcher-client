<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/src/tool/debug.php';

/**
 * Interní API, které slouží pro komunikaci se serverem.
 * ====================================================================================================================
 */
abstract class FileFetcherApi {
    
    private
        $urlBuilder,
        $options;
    
    protected function __construct(FileFetcherApiOptions $options) {
        $this->urlBuilder = new FileFetcherUrlBuilder($options->endpoint, $options->tokenPattern);
        $this->options = $options;
    }
    
    protected final function callRoot() : FileFetcherResponse {
        $target = $this->urlBuilder->rootUrl();
        return $this->sendRequest($target);
    }
    
    protected final function callStats() : FileFetcherResponse {
        $target = $this->urlBuilder->statsUrl();
        return $this->sendRequest($target);
    }
    
    protected final function callCategories(?string $categoryGuid = null, ?string $login = null, ?string $password = null) : FileFetcherResponse {
        $target = $this->urlBuilder->categoryUrl($categoryGuid, $login, $password);
        return $this->sendRequest($target);
    }

    protected final function callFileAccess(?string $fileGuid, ?string $login = null, ?string $password = null) : ?FileFetcherResponse {
        if (empty($fileGuid)) {
            return null;
        }
        $target = $this->urlBuilder->fileAccessUrl($fileGuid, $login, $password);
        return $this->sendRequest($target);
    }

    protected final function callFileInfo(?string $fileGuid, ?string $login = null, ?string $password = null) : ?FileFetcherResponse {
        if (empty($fileGuid)) {
            return null;
        }
        $target = $this->urlBuilder->fileInfoUrl($fileGuid, $login, $password);
        return $this->sendRequest($target);
    }
    
    protected final function callFileImageUrl(?string $fileGuid, ?int $width = null, ?int $height = null, ?float $rotate = 0., ?string $login = null, ?string $password = null) : ?FileFetcherUrlData {
        if (empty($fileGuid)) {
            return null;
        }
        return $this->urlBuilder->fileImageUrl($fileGuid, $width, $height, $rotate, $login, $password);
    }
    
    protected final function callFileDownloadUrl(?string $fileGuid, ?string $login = null, ?string $password = null) : ?FileFetcherUrlData {
        if (empty($fileGuid)) {
            return null;
        }
        return $this->urlBuilder->fileDownloadUrl($fileGuid, $login, $password);
    }
    
    protected final function callFileOpenUrl(?string $fileGuid, ?string $login = null, ?string $password = null) : ?FileFetcherUrlData {
        if (empty($fileGuid)) {
            return null;
        }
        return $this->urlBuilder->fileOpenUrl($fileGuid, $login, $password);
    }
    
    protected final function callRandomFileInfo(?string $categoryGuid = null, ?int $count = 1) : FileFetcherResponse {
        $target = $this->urlBuilder->randomFileInfoUrl($categoryGuid, $count);
        return $this->sendRequest($target);
    }

    protected final function callChangePassword(?string $login, ?string $password, ?string $newPassword) : FileFetcherResponse {
        $target = $this->urlBuilder->changePasswordUrl($login, $password, $newPassword);
        return $this->sendRequest($target);
    }
    
    private function sendRequest(FileFetcherUrlData $urlData) : FileFetcherResponse {
        $options = self::prepareStreamContextOptions($urlData->params);
        $context = stream_context_create($options);
        $response = @file_get_contents($urlData->absoluteUrl, false, $context);
        $success = ($response !== false);
        $isJson = false;
        
        if ($success) {
            $isJson = self::tryConvertToJsonObject($response);
        }
        if ($this->options->debugMode) {
            $result = (object) [
                'url'      => $urlData->baseUrl,
                'params'   => $urlData->params,
                'options'  => $options,
                'response' => $isJson ? $response : '[bytes]',
                'success'  => $success
            ];
            Debug::show($result);
        }
        return new FileFetcherResponse($success, $success ? $response : null);
    }
    
    private static function prepareStreamContextOptions(array $params) : array {
        return [
            'http' => [
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'method'  => 'GET',
                'content' => http_build_query($params)
            ]
        ];
    }
    
    private static function tryConvertToJsonObject(&$response) : bool {
        if ($response != null && is_string($response)) {
            $jsonObj = json_decode($response);
            
            // JSON není validní nebo response neobsahuje JSON
            if ($jsonObj !== null) {
                $response = $jsonObj;
                return true;
            }
        } else {
            $response = null;
        }
        return false;
    }
}

/**
 * Nastavení pro API.
 * ====================================================================================================================
 */
final class FileFetcherApiOptions {

    public
        $endpoint,
        $tokenPattern,
        $debugMode;
}

/**
 * Sestaví URL pro komunikaci se serverem.
 * Zajišťuje vytvoření i tokenu uživatele dle přihlašovacích údajů.
 * ====================================================================================================================
 */
final class FileFetcherUrlBuilder {

    private const PARAM_TOKEN = 'token';

    private
        $endpoint,
        $tokenPattern;

    public function __construct(string $endpoint, array $tokenPattern) {
        $this->endpoint = $endpoint;
        $this->tokenPattern = $tokenPattern;
    }

    public function rootUrl() : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint, []);
    }

    public function statsUrl() : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/stats', []);
    }
    
    public function categoryUrl(?string $categoryGuid, ?string $login, ?string $password) : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/cat', [
            self::PARAM_TOKEN => $this->createAuthToken($login, $password),
            'guid'            => $categoryGuid
        ]);
    }

    public function fileAccessUrl(?string $fileGuid, ?string $login, ?string $password) : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/file/fetch/'.$fileGuid.'/access', [
            self::PARAM_TOKEN => $this->createAuthToken($login, $password)
        ]);
    }

    public function fileInfoUrl(?string $fileGuid, ?string $login, ?string $password) : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/file/fetch/'.$fileGuid.'/info', [
            self::PARAM_TOKEN => $this->createAuthToken($login, $password)
        ]);
    }
    
    public function fileImageUrl(?string $fileGuid, ?int $width, ?int $height, ?float $rotate, ?string $login, ?string $password) : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/file/fetch/'.$fileGuid.'/image', [
            self::PARAM_TOKEN  => $this->createAuthToken($login, $password),
            'width'            => $width,
            'height'           => $height,
            'rotate'           => $rotate
        ]);
    }
    
    public function fileDownloadUrl(?string $fileGuid, ?string $login, ?string $password) : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/file/fetch/'.$fileGuid.'/download', [
            self::PARAM_TOKEN => $this->createAuthToken($login, $password)
        ]);
    }
    
    public function fileOpenUrl(?string $fileGuid, ?string $login, ?string $password) : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/file/fetch/'.$fileGuid.'/open', [
            self::PARAM_TOKEN => $this->createAuthToken($login, $password)
        ]);
    }
    
    public function randomFileInfoUrl(?string $categoryGuid, ?int $count) : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/file/random/info', [
            'category' => $categoryGuid,
            'count'    => $count
        ]);
    }

    public function changePasswordUrl(?string $login, ?string $password, ?string $newPassword) : FileFetcherUrlData {
        return new FileFetcherUrlData($this->endpoint.'/user/change-password-token', [
            self::PARAM_TOKEN => $this->createAuthToken($login, $password),
            'newPassword'     => $this->hashPassword($newPassword),
        ]);
    }

    private function createAuthToken(?string $login, ?string $password) : ?string {
        if (!empty($login) && !empty($password)) {
            $passwordHash = $this->hashPassword($password);
            $token = $login.':'.$passwordHash;
            $token = self::toUrlBase64($token);
            $token = self::mixBase64($token, $this->tokenPattern);
            return $token;
        }
        return null;
    }

    private function hashPassword(?string $password) : ?string {
        return hash(DEF_PASSWORD_HASH_ALG, $password);
    }
    
    private static function toUrlBase64(?string $data) : string {
        if (empty($data)) {
            return '';
        }
        $base64 = base64_encode($data);
        
        if ($base64 === false) {
            return false;
        }
        $url = strtr($base64, '+/', '-_');
        return rtrim($url, '=');
    }
    
    private static function mixBase64(?string $base64, array $tokenPattern) : string {
        $origin = $tokenPattern[0];
        $mixed = $tokenPattern[1];
        $out = '';

        if (empty($base64)) {
            return $out;
        }
        for ($i = 0; $i < strlen($base64); $i++) {
            $found = false;
            $j = 0;
            
            for (; $j < strlen($origin); $j++) {
                if ($origin[$j] == $base64[$i]) {
                    $found = true;
                    break;
                }
            }
            $out .= $found ? $mixed[$j] : $base64[$i];
        }
        return $out;
    }
}

/**
 * Informace o sestavené URL, která bude směřovat na server.
 * ====================================================================================================================
 */
final class FileFetcherUrlData {

    public
        $baseUrl,     // základní URL bez parametrů
        $params,      // parametry
        $absoluteUrl; // absolutní URL s parametry
        
    public function __construct(string $baseUrl, ?array $params) {
        $this->baseUrl = $baseUrl;
        $this->params = $params;
        
        if (!empty($params)) {
            $query = http_build_query($params);
            $this->absoluteUrl = $baseUrl.'?'.$query;
        } else {
            $this->absoluteUrl = $baseUrl;
            $this->params = [];
        }
    }
}

/**
 * Odpověď ze serveru na sestavenou URL.
 * ====================================================================================================================
 */
final class FileFetcherResponse {

    public
        $success, // odpověď byla úšpěšná (většinou stav 200)
        $data;    // přijatá data z response (může být cokoliv)
        
    public function __construct(bool $success, $data = null) {
        $this->success = $success;
        $this->data = $data;
    }
}
