<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/settings.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/src/tool/token-provider.php';
require_once dirname(__FILE__).'/server-api-wrapper.php';

/**
 * Resolver pro vlastní API wrapper, který slouží pro komunikaci se serverem.
 * Obsahuje metody, které volají server a získají už DTO objekty nebo přímo výstup souboru v podobě zobrazení/streamu atd.
 * ====================================================================================================================
 */
final class FileFetcherApiWrapperResolver {

    private const
        // hlavičky
        HEADER_CONTENT_TYPE              = 'Content-Type',
        HEADER_CONTENT_LENGTH            = 'Content-Length',
        HEADER_CONTENT_DISPOSITION       = 'Content-Disposition',
        HEADER_CONTENT_DESCRIPTION       = 'Content-Description',
        HEADER_CONTENT_TRANSFER_ENCODING = 'Content-Transfer-Encoding',
        HEADER_EXPIRES                   = 'Expires',
        HEADER_CONNETION                 = 'Connection',
        HEADER_CACHE_CONTROL             = 'Cache-Control',
        HEADER_PRAGMA                    = 'Pragma',
        HEADER_ACCEPT_RANGES             = 'Accept-Ranges',
        // nový hlavičky na serveru
        HEADER_FILE_ERROR                = 'File-Error',
        HEADER_FILE_EXTENSION            = 'File-Extension',
        HEADER_FILE_CHECKSUM             = 'File-Checksum';

    public const
        // parametry
        PARAM_FILE_GUID        = 'f',
        PARAM_FILE_CHECKSUM    = 's',
        PARAM_CATEGORY_GUID    = 'c',
        PARAM_PREVIEW_MODE     = 'pm',
        PARAM_IMAGE_WIDTH      = 'iw',
        PARAM_IMAGE_HEIGHT     = 'ih',
        // enum typy
        TYPE_ROOT              = 'root',
        TYPE_STATS             = 'stats',
        TYPE_CAT               = 'cat',
        TYPE_FILE_ACCESS       = 'file_access',
        TYPE_FILE_INFO         = 'file_info',
        TYPE_FILE_IMAGE        = 'file_image',
        TYPE_FILE_DOWNLOAD     = 'file_download',
        TYPE_FILE_OPEN         = 'file_open',
        TYPE_RANDOM_FILE_INFO  = 'random_file_info',
        // enum typy náhledu obrázku do cache
        PM_THUMB               = 0,
        PM_LARGE               = 1,
        PM_ORIGIN              = 2;

    private
        $apiWrapper,
        $tokenProvider,
        $token,
        $login,
        $password;

    public function __construct() {
        // instance API wrapperu, přes který se volá server
        $options = new FileFetcherApiOptions();
        $options->debugMode = DEF_DEBUG_MODE;
        $options->endpoint = DEF_ENDPOINT;
        $options->tokenPattern = DEF_TOKEN_PATTERN;
        $this->apiWrapper = new FileFetcherApiWrapper($options);

        // token provider slouží na získání uživatele z URL
        $this->tokenProvider = new TokenProvider(DEF_SECRET_KEY, DEF_SECRET_KEY_TTL, DEF_REFRESH_EXPIRE_TIME);
        $this->token = $this->tokenProvider->decryptUrlToken();

        if (!$this->token->expired) {
            $this->login = $this->token->login;
            $this->password = $this->token->password;
        }
    }

    public function login(?string $login, ?string $password) : ?object {
        $token = $this->tokenProvider->generateToken($login, $password);
        $token = $this->tokenProvider->decryptToken($token);
        return $token;
    }

    public function changePassword(?string $login, ?string $password, ?string $newPassword) : ?object {
        $token = $this->token;
        $response = $this->apiWrapper->changePassword($login, $password, $newPassword);

        if ($response->changed) {
            $token = $this->tokenProvider->generateToken($login, $newPassword);
            $token = $this->tokenProvider->decryptToken($token);
        }
        return (object) [
            'token'   => $token,
            'changed' => $response->changed
        ];
    }

    public function getToken() : ?object {
        return $this->token;
    }

    public function execute(string $type, ?array $params = [], ?callable $callback = null) {
        switch ($type) {
            case self::TYPE_ROOT:
                return $this->apiWrapper->getRoot();

            case self::TYPE_STATS:
                return $this->apiWrapper->getStats();

            case self::TYPE_CAT:
                @list('categoryGuid' => $categoryGuid) = $params;

                if ($categoryGuid == null) {
                    $categoryGuid = self::readUrlParam(self::PARAM_CATEGORY_GUID);
                }
                return $this->apiWrapper->getCategories($categoryGuid, $this->login, $this->password);

            case self::TYPE_FILE_ACCESS:
                @list('fileGuid' => $fileGuid) = $params;
                return $this->apiWrapper->getFileAccess($fileGuid, $this->login, $this->password);

            case self::TYPE_FILE_INFO:
                @list('fileGuid' => $fileGuid) = $params;
                return $this->apiWrapper->getFileInfo($fileGuid, $this->login, $this->password);

            case self::TYPE_FILE_IMAGE:
                @list('fileGuid'     => $fileGuid,
                      'checksum'     => $checksum,
                      'width'        => $width,
                      'height'       => $height,
                      'rotate'       => $rotate,
                      'mode'         => $mode
                ) = $params;

                if ($fileGuid == null) {
                    $fileGuid = self::readUrlParam(self::PARAM_FILE_GUID);
                }
                if ($checksum == null) {
                    $checksum = self::readUrlParam(self::PARAM_FILE_CHECKSUM);
                }
                if ($width == null) {
                    $width = self::readUrlParam(self::PARAM_IMAGE_WIDTH);
                }
                if ($height == null) {
                    $height = self::readUrlParam(self::PARAM_IMAGE_HEIGHT);
                }
                if ($mode == null) {
                    $mode = self::readUrlParam(self::PARAM_PREVIEW_MODE);
                }
                // výchozí mód bude thumb
                if ($mode == null || !is_numeric($mode) || !in_array($mode, [self::PM_THUMB, self::PM_LARGE, self::PM_ORIGIN])) {
                    $mode = self::PM_THUMB;
                }
                // jedná se o původní velikost náhledu
                if ($mode == self::PM_ORIGIN) {
                    $width = null;
                    $height = null;
                } else {
                    // pokud nebudou nastaveny vlastní rozměry (původní logika)
                    if ($mode == self::PM_THUMB && $width == null && $height == null) {
                        $width = DEF_PREVIEW_SIZE;
                    } else if ($mode == self::PM_LARGE && $width == null && $height == null) {
                        $width = DEF_LARGE_PREVIEW_SIZE;
                    }
                    // pokud budou nastaveny rozměry
                    else {
                        if ($width > $height) {
                            $height = null;

                            if ($mode == self::PM_THUMB && $width > DEF_PREVIEW_SIZE) {
                                $width = DEF_PREVIEW_SIZE;
                            } else if ($mode == self::PM_LARGE && $width > DEF_LARGE_PREVIEW_SIZE) {
                                $width = DEF_LARGE_PREVIEW_SIZE;
                            }
                        } else {
                            $width = null;

                            if ($mode == self::PM_THUMB && $height > DEF_PREVIEW_SIZE) {
                                $height = DEF_PREVIEW_SIZE;
                            } else if ($mode == self::PM_LARGE && $height > DEF_LARGE_PREVIEW_SIZE) {
                                $height = DEF_LARGE_PREVIEW_SIZE;
                            }
                        }
                    }
                }
                $target = $this->apiWrapper->getFileImageUrl($fileGuid, $width, $height, $rotate, $this->login, $this->password);
                $callbackData = (object) [
                    'fileGuid'     => $fileGuid,
                    'fileChecksum' => $checksum,
                    'mode'         => $mode
                ];
                $this->fileImageProcess($target, $callback, $callbackData);
                exit;

            case self::TYPE_FILE_DOWNLOAD:
                @list('fileGuid' => $fileGuid) = $params;

                if ($fileGuid == null) {
                    $fileGuid = self::readUrlParam(self::PARAM_FILE_GUID);
                }
                $target = $this->apiWrapper->getFileDownloadUrl($fileGuid, $this->login, $this->password);

                $this->fileDownloadOrOpenProcess($target, $fileGuid);
                exit;

            case self::TYPE_FILE_OPEN:
                @list('fileGuid' => $fileGuid) = $params;

                if ($fileGuid == null) {
                    $fileGuid = self::readUrlParam(self::PARAM_FILE_GUID);
                }
                $target = $this->apiWrapper->getFileOpenUrl($fileGuid, $this->login, $this->password);

                self::fileDownloadOrOpenProcess($target, $fileGuid);
                exit;

            case self::TYPE_RANDOM_FILE_INFO:
                @list('categoryGuid' => $categoryGuid,
                      'count'        => $count
                ) = $params;

                if ($categoryGuid == null) {
                    $categoryGuid = self::readUrlParam(self::PARAM_CATEGORY_GUID);
                }
                return $this->apiWrapper->getRandomFileInfo($categoryGuid, $count);
        }
        return null;
    }

    public static function readUrlParam(string $paramName) : ?string {
        if (array_key_exists($paramName, $_GET)) {
            $value = $_GET[$paramName];
            return (isset($value) && !empty($value)) ? $value : null;
        }
        return null;
    }

    private static function setDefaultHeaders() {
        header(self::HEADER_CONTENT_DESCRIPTION.': File Transfer');
        header(self::HEADER_CONTENT_TRANSFER_ENCODING.': binary');
        header(self::HEADER_EXPIRES.': 0');
        header(self::HEADER_CONNETION.': Keep-Alive');
        header(self::HEADER_CACHE_CONTROL.': must-revalidate, post-check=0, pre-check=0');
        header(self::HEADER_PRAGMA.': public');
    }

    private function fileImageProcess(?FileFetcherUrlData $target, callable $callback, object $callbackData) {
        $handle = false;
        $fileExtension = DEF_FAILBACK_PREVIEW_EXTENSION;
        $callbackData->fileExtension = $fileExtension;

        // první volání callback je před zavoláním serveru (jsou dostupné pouze vstupní parametry)
        // pozor: parametry pro callback nesmí obsahovat handle!
        if ($callback($callbackData)) {
            $handle = @fopen($target->absoluteUrl, 'r');
            $headers = self::readHeadersValues(@$http_response_header); // hlavičky budou v případě, když server bude online
            $callbackData->handle = $handle;
            $callbackData->fileError = $headers[self::HEADER_FILE_ERROR];
            $callbackData->fileChecksum = $headers[self::HEADER_FILE_CHECKSUM];
            $callbackData->fileExtension = $headers[self::HEADER_FILE_EXTENSION];

            // druhé volání callbacku bude po zavolání serveru
            $callback($callbackData);
            $handle = $callbackData->handle;
            $fileExtension = $callbackData->fileExtension;
        }
        // v callbacku bylo zastaveno zavolání serveru a byl nastaven vlastni stream na soubor
        else if (isset($callbackData->handle)) {
            $handle = $callbackData->handle;
        }
        // handle bude (musí) vždy existovat
        $stat = fstat($handle);
        $fileSize = $stat['size'];

        self::updateHeaders([
            self::HEADER_CONTENT_TYPE.': image/'.$fileExtension,
            self::HEADER_CONTENT_LENGTH.': '.$fileSize
        ]);
        fpassthru($handle);
        fclose($handle);
    }

    private function fileDownloadOrOpenProcess(?FileFetcherUrlData $target, ?string $fileGuid) {
        $handle = @fopen($target->absoluteUrl, 'r');

        if ($handle !== false) {
            $fileAccess = $this->execute(FileFetcherApiWrapperResolver::TYPE_FILE_ACCESS, [
                'fileGuid' => $fileGuid
            ]);
            if ($fileAccess != null && $fileAccess->available) {
                self::updateHeaders($http_response_header);
                fpassthru($handle);
            }
            fclose($handle);
        } else {
            header('Location: /');
        }
    }

    private static function updateHeaders(array $headers) {
        self::setDefaultHeaders();
        $contentLength = 0;

        foreach ($headers as $key) {
            if (strpos($key, self::HEADER_CONTENT_TYPE.': ') !== false) {
                header($key);
                continue;
            }
            if (strpos($key, self::HEADER_CONTENT_LENGTH.': ') !== false) {
                $contentLength = (int) substr($key, strlen(self::HEADER_CONTENT_LENGTH.': '));
                header($key);
                continue;
            }
            if (strpos($key, self::HEADER_CONTENT_DISPOSITION.': ') !== false) {
                header($key);
                continue;
            }
        }
        if ($contentLength > 0) {
            header(self::HEADER_ACCEPT_RANGES.': bytes 0-'.$contentLength);
        }
    }

    private static function readHeadersValues(?array $headers) : array {
        // výchozí hodnoty musí existovat
        $values = [
            self::HEADER_FILE_CHECKSUM  => 0,
            self::HEADER_FILE_EXTENSION => DEF_FAILBACK_PREVIEW_EXTENSION,
            self::HEADER_FILE_ERROR     => false
        ];
        if (isset($headers)) {
            foreach ($headers as $key) {
                if (strpos($key, self::HEADER_FILE_CHECKSUM.': ') !== false) {
                    $values[self::HEADER_FILE_CHECKSUM] = (int) substr($key, strlen(self::HEADER_FILE_CHECKSUM.': '));
                    continue;
                }
                if (strpos($key, self::HEADER_FILE_EXTENSION.': ') !== false) {
                    $values[self::HEADER_FILE_EXTENSION] = (string) substr($key, strlen(self::HEADER_FILE_EXTENSION.': '));
                    continue;
                }
                if (strpos($key, self::HEADER_FILE_ERROR.': ') !== false) {
                    $values[self::HEADER_FILE_ERROR] = (boolean) substr($key, strlen(self::HEADER_FILE_ERROR.': '));
                    continue;
                }
            }
        }
        return $values;
    }
}
