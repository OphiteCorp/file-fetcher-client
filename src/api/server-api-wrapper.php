<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/src/localizer/localizer.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/src/tool/utils.php';
require_once dirname(__FILE__).'/server-api.php';

/**
 * Vlastní wrapper, který obalí základní API pro komunikaci se serverem.
 * Jako response se vrací vlastní DTO objekty.
 * ====================================================================================================================
 */
final class FileFetcherApiWrapper extends FileFetcherApi {
    
    public function __construct(FileFetcherApiOptions $options) {
        parent::__construct($options);
    }
    
    public function getRoot() : RootResponseDto {
        $response = parent::callRoot();
        return RootResponseDto::mapFromResponse($response);
    }
    
    public function getStats() : StatsResponseDto {
        $response = parent::callStats();
        return StatsResponseDto::mapFromResponse($response);
    }
    
    public function getCategories(?string $categoryGuid = null, ?string $login = null, ?string $password = null) : CategoryResponseDto {
        $response = parent::callCategories($categoryGuid, $login, $password);
        return CategoryResponseDto::mapFromResponse($response);
    }

    public function getFileAccess(?string $fileGuid, ?string $login = null, ?string $password = null) : FileAccessResponseDto {
        $response = parent::callFileAccess($fileGuid, $login, $password);
        return FileAccessResponseDto::mapFromResponse($response);
    }

    public function getFileInfo(?string $fileGuid, ?string $login = null, ?string $password = null) : array {
        $response = parent::callFileInfo($fileGuid, $login, $password);
        return FileResponseDto::mapFromResponse($response);
    }
    
    public function getFileImageUrl(?string $fileGuid, ?int $width = null, ?int $height = null, ?float $rotate = 0., ?string $login = null, ?string $password = null) : ?FileFetcherUrlData {
        return parent::callFileImageUrl($fileGuid, $width, $height, $rotate, $login, $password);
    }
    
    public function getFileDownloadUrl(?string $fileGuid, ?string $login = null, ?string $password = null) : ?FileFetcherUrlData {
        return parent::callFileDownloadUrl($fileGuid, $login, $password);
    }
    
    public function getFileOpenUrl(?string $fileGuid, ?string $login = null, ?string $password = null) : ?FileFetcherUrlData {
        return parent::callFileOpenUrl($fileGuid, $login, $password);
    }
    
    public function getRandomFileInfo(?string $categoryGuid = null, ?int $count = 1) : array {
        $response = parent::callRandomFileInfo($categoryGuid, $count);
        return FileResponseDto::mapFromResponse($response);
    }

    public function changePassword(?string $login, ?string $password, ?string $newPassword) : ChangePasswordDto {
        $response = parent::callChangePassword($login, $password, $newPassword);
        return ChangePasswordDto::mapFromResponse($login, $newPassword, $response);
    }
}

/**
 * Response objekt pro změnu hesla.
 * ====================================================================================================================
 */
final class ChangePasswordDto {

    public
        $login,
        $newPassword,
        $changed = false;

    public static function mapFromResponse(?string $login, ?string $newPassword, ?FileFetcherResponse $resp) : ChangePasswordDto {
        $dto = new ChangePasswordDto();
        $dto->login = $login;
        $dto->newPassword = $newPassword;

        if (isset($resp) && $resp->success) {
            $data = $resp->data;

            $dto->changed = $data;
        }
        return $dto;
    }
}

/**
 * Response objekt pro volání root.
 * ====================================================================================================================
 */
final class RootResponseDto {
    
    public
        $message = '',
        $version = '';
        
    public static function mapFromResponse(?FileFetcherResponse $resp) : RootResponseDto {
        $dto = new RootResponseDto();
        
        if (isset($resp) && $resp->success) {
            $data = $resp->data;
            
            $dto->message = $data->message;
            $dto->version = $data->version;
        }
        return $dto;
    }
}

/**
 * Response objekt pro volání statistiky.
 * ====================================================================================================================
 */
final class StatsResponseDto {
    
    public
        $totalCategories = 0,
        $totalFiles = 0,
        $totalImages = 0,
        $totalVideos = 0,
        $totalFilesSize = 0,
        $totalFilesSizeReadable = '',
        $totalHiddenFiles = 0,
        $totalUsers = 0,
        $uptime = '';
        
    public static function mapFromResponse(?FileFetcherResponse $resp) : StatsResponseDto {
        $dto = new StatsResponseDto();
        
        if (isset($resp) && $resp->success) {
            $data = $resp->data;
            
            $dto->totalCategories = $data->total_categories;
            $dto->totalFiles = $data->total_files;
            $dto->totalImages = $data->total_images;
            $dto->totalVideos = $data->total_videos;
            $dto->totalFilesSize = $data->total_files_size;
            $dto->totalFilesSizeReadable = Utils::formatSize($data->total_files_size);
            $dto->totalHiddenFiles = $data->total_hidden_files;
            $dto->totalUsers = $data->total_users;
            $dto->uptime = Utils::toElapsedTime($data->uptime);
        }
        return $dto;
    }
}

/**
 * Response objekt pro volání seznamu kategorií, které obsahují i soubory (hlavní objekt galerie).
 * ====================================================================================================================
 */
final class CategoryResponseDto {

    public
        $categories = [], // kategorie případně podkategorie
        $files      = [], // soubory v kategorii
        $paths      = []; // cesta od root po aktuální kategorii
    
    public function isEmpty() : bool {
        return (empty($this->categories) && empty($this->files));
    }
    
    public static function mapFromResponse(?FileFetcherResponse $resp) : CategoryResponseDto {
        $dto = new CategoryResponseDto();
        $defaultPaths = CategoryPathDto::mapFromObject('', null);
        
        if (isset($resp) && $resp->success) {
            $data = $resp->data;

            if (isset($data->categories)) {
                foreach ($data->categories as $category) {
                    $dto->categories[] = CategoryDto::mapFromObject($category);
                }
            }
            if (isset($data->files)) {
                foreach ($data->files as $file) {
                    $dto->files[] = FileResponseDto::mapFromObject($file);
                }
            }
            if (isset($data->paths)) {
                foreach ($data->paths as $guid => $customData) {
                    $dto->paths[] = CategoryPathDto::mapFromObject($guid, $customData);
                }
            } else {
                $dto->paths[] = $defaultPaths;
            }
        } else {
            $dto->paths[] = $defaultPaths;
        }
        return $dto;
    }
}

/**
 * Response objekt pro informace o přístupu a oprávnění na soubor.
 * ====================================================================================================================
 */
final class FileAccessResponseDto {

    public
        $guid = null,
        $available = false;

    public static function mapFromResponse(?FileFetcherResponse $resp) : FileAccessResponseDto {
        $dto = new FileAccessResponseDto();

        if (isset($resp) && $resp->success) {
            $data = $resp->data;

            $dto->guid = $data->guid;
            $dto->available = $data->available;
        }
        return $dto;
    }
}

/**
 * Response objekt pro informace o souboru.
 * ====================================================================================================================
 */
final class FileResponseDto {
    
    public const ET_IMAGE   = 'image';
    public const ET_VIDEO   = 'video';
    public const ET_YOUTUBE = 'youtube';
    
    public
        $guid = null,
        $name,
        $description,
        $author,
        $createdRecord,
        $createdFile,
        $extension,
        $mimeType,
        $size,
        $sizeReadable,
        $checksum,
        $grade,
        $hidden,
        $exists,
        $newlyAdded,
        $newlyAddedLeft,
        $extraType,
        // image/video
        $imgWidth,
        $imgHeight,
        // video
        $videoDuration;
        
    public static function mapFromResponse(?FileFetcherResponse $resp) : array {
        $output = [];

        if (isset($resp) && $resp->success) {
            $data = $resp->data;
            
            if (is_array($data)) {
                foreach ($data as $row) {
                    $output[] = self::mapFromObject($row);
                }
                return $output;
            } else {
                $obj = self::mapFromObject($data);
                if ($obj != null) {
                    $output[] = $obj;
                }
            }
        }
        return $output;
    }
    
    public static function mapFromObject(?object $file) : ?FileResponseDto {
        if (!isset($file) || !isset($file->guid)) {
            return null;
        }
        $dto = new FileResponseDto();
        $dto->guid = $file->guid;
        $dto->name = isset($file->name) ? $file->name : null;
        $dto->description = isset($file->desc) ? $file->desc : null;
        $dto->author = isset($file->author) ? $file->author : null;
        $dto->createdRecord = Utils::toDateTimeFormat($file->created_record);
        $dto->extension = isset($file->ext) ? $file->ext : null;
        $dto->mimeType = isset($file->mime_type) ? $file->mime_type : null;
        $dto->size = isset($file->size) ? $file->size : 0;
        $dto->sizeReadable = Utils::formatSize($dto->size);
        $dto->checksum = isset($file->checksum) ? $file->checksum : null;
        $dto->grade = isset($file->grade) ? $file->grade : null;
        $dto->hidden = $file->hidden;
        $dto->exists = $file->exists;
        $dto->newlyAdded = $file->newly_added;
        $dto->newlyAddedLeft = Utils::toElapsedTime($file->newly_added_left);
        
        if ($dto->exists) {
            $dto->extraType = isset($file->extra_type) ? $file->extra_type : null;
            $dto->createdFile = Utils::toDateTimeFormat($file->created_file);
        }
        if ($dto->extraType != null) {
            $extra = $file->extra_data;
            
            switch ($dto->extraType) {
                case self::ET_IMAGE:
                    $dto->imgWidth = $extra->width;
                    $dto->imgHeight = $extra->height;
                    break;
                
                case self::ET_VIDEO:
                    $dto->imgWidth = $extra->width;
                    $dto->imgHeight = $extra->height;
                    $dto->videoDuration = $extra->duration;
                    break;
                   
                case self::ET_YOUTUBE:
                    // zatím nic
                    break;
            }
        }
        return $dto;
    }
}

/**
 * Pomocný objekt s informacemi o jedné kategorií.
 * ====================================================================================================================
 */
 final class CategoryDto {

    public
        $guid = null,
        $name = null,
        $description = null,
        $created = null,
        $newlyAdded = false,
        $newlyAddedLeft = null,
        $protected = true,
        $accessAllowed = false,
        $filesCount = 0,
        $subCategoriesCount = 0,
        $totalFilesCount = 0,
        $totalCategorySize = 0,
        $totalCategorySizeReadable = null,
        $previewFile = null;

    public function isEmpty() : bool {
        return ($this->subCategoriesCount == 0 && $this->totalFilesCount == 0);
    }

    public static function mapFromObject(object $category) : CategoryDto {
        $dto = new CategoryDto();
        $dto->guid = $category->guid;
        $dto->name = $category->name;
        $dto->description = isset($category->desc) ? $category->desc : null;
        $dto->created = Utils::toDateFormat($category->created);
        $dto->newlyAdded = $category->newly_added;
        $dto->newlyAddedLeft = Utils::toElapsedTime($category->newly_added_left);
        $dto->protected = $category->protected;
        $dto->accessAllowed = $category->access_allowed;
        $dto->filesCount = isset($category->files_count) ? $category->files_count : null;
        $dto->subCategoriesCount = isset($category->sub_categories_count) ? $category->sub_categories_count : null;
        $dto->totalFilesCount = isset($category->total_files_count) ? $category->total_files_count : null;
        $dto->totalCategorySize = isset($category->total_category_size) ? $category->total_category_size : 0;
        $dto->totalCategorySizeReadable = Utils::formatSize($dto->totalCategorySize);
        
        if (isset($category->preview_file)) {
            $dto->previewFile = CategoryPreviewFileDto::mapToPreviewFile($category->preview_file);
        }
        return $dto;
    }
}

/**
 * Pomocný objekt s informacemi o náhledu kategorie.
 * ====================================================================================================================
 */
final class CategoryPreviewFileDto {
    
    public
        $guid = null,
        $checksum = 0,
        $width = 0,
        $height = 0;
        
    public static function mapToPreviewFile(object $obj) : CategoryPreviewFileDto {
        $file = new CategoryPreviewFileDto();
        $file->guid = $obj->guid;
        $file->checksum = $obj->checksum;
        $file->width = $obj->width;
        $file->height = $obj->height;
        return $file;
    }
}

/**
 * Pomocný objekt s informacemi o cestě k aktuální kategorii.
 * ====================================================================================================================
 */
final class CategoryPathDto {
    
    public
        $guid = null,
        $name = null,
        $description = null;
        
    public static function mapFromObject(?string $guid, $customData) : CategoryPathDto {
        $dto = new CategoryPathDto();
        $dto->guid = $guid;
        $dto->description = null;
        
        // nastane v případě, že uživatel nemá oprávnění do kategorie
        if (empty($guid) || is_string($customData)) {
            $dto->name = Localizer::instance()->tr(LC::HOME);
        } else {
            $dto->name = ($customData->name == '/') ? Localizer::instance()->tr(LC::HOME) : $customData->name;
            $dto->description = isset($customData->desc) ? $customData->desc : null;
        }
        return $dto;
    }
}
