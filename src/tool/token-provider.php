<?php

/**
 * Řeší encrypt a decrypt tokenu v URL.
 * Token slouží pro předání informací v URL jako je jméno a heslo.
 */
final class TokenProvider {

    public const PARAM_TOKEN = 'as';

    private
        $secretKey,
        $ttl,
        $refreshExpireTime,
        $token;

    public function __construct(string $secretKey, int $ttl, bool $refreshExpireTime) {
        $this->secretKey = $secretKey;
        $this->ttl = $ttl;
        $this->refreshExpireTime = $refreshExpireTime;

        $param = array_key_exists(self::PARAM_TOKEN, $_REQUEST) ? $_REQUEST[self::PARAM_TOKEN] : null;

        if (isset($param) && !empty($param)) {
            $token = trim($param);

            // validní token bude mít kolem 80 až 150 znaků
            // je ho potřeba takto oříznout, aby zbytečně neproběhl pokus o decrypt
            if (!empty($token) && strlen($token) <= 300) {
                $this->token = $token;
            }
        }
    }

    public function generateToken(?string $login, ?string $password, ?object $options = null) : string {
        if (empty($login) || empty($password)) {
            return '';
        }
        $permanent = ($this->ttl === 0);
        $startTime = time() + $this->ttl;
        $time = $startTime;

        if ($options != null) {
            if (isset($options->permanent)) {
                $permanent = $options->permanent;
            }
            if (isset($options->time)) {
                $time = $options->time;
            }
        }
        $obj = [$login, $password, $permanent ? 1 : 0];

        if (!$permanent) {
            if ($this->refreshExpireTime) {
                $time = $startTime;
            }
            $obj[] = $time;
        }
        $data = json_encode($obj);
        return self::encrypt($data, $this->secretKey);
    }

    public function decryptUrlToken() : object {
        return $this->decryptToken($this->token);
    }

    public function decryptToken(?string $token) : object {
        // výchozí hodnoty neměnit
        $result = (object) [
            'login'         => null,
            'password'      => null,
            'remainingTime' => 0,
            'expired'       => false,
            'permanent'     => false,
            'nextToken'     => null
        ];
        $decrypted = self::decrypt($token, $this->secretKey);
        // vstupní data se podařilo decryptovat
        if ($decrypted != null) {
            $data = json_decode($decrypted);
            $result->permanent = (bool) $data[2];

            // pokud token bude obsahovat expiraci, tak následující hodnota je čas vypršení
            if (!$result->permanent) {
                $result->remainingTime = max(0, $data[3] - time());
                $result->expired = ($result->remainingTime === 0);
            }
            // pokud bude token platný, tak nastavíme vše ostatní a vygenerujeme další token
            if (!$result->expired) {
                $options = ['permanent' => $result->permanent];

                if (!$result->permanent) {
                    $options['time'] = $data[3];
                }
                $result->login = $data[0];
                $result->password = $data[1];
                $result->nextToken = $this->generateToken($result->login, $result->password, (object) $options);
            }
        }
        return $result;
    }

    private static function encrypt(string $data, string $secretKey) : string {
        $cipher = 'aes-256-gcm';
        $ivLen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivLen);
        $encrypt = openssl_encrypt($data, $cipher, $secretKey, $options = 0, $iv, $tag);
        $encrypt = base64_decode($encrypt);
        return self::toUrlBase64($iv . $encrypt . $tag);
    }

    private static function decrypt(?string $data, string $secretKey) : ?string {
        if ($data == null || strlen($data) < 28) {
            return null;
        }
        $cipher = 'aes-256-gcm';
        $data = self::fromUrlBase64($data);
        $iv = substr($data, 0, 12);
        $encrypt = base64_encode(substr($data, 12, strlen($data) - 12 - 16));
        $tag = substr($data, strlen($data) - 16);
        $decrypt = openssl_decrypt($encrypt, $cipher, $secretKey, $options = 0, $iv, $tag);
        return $decrypt ? $decrypt : null;
    }

    private static function toUrlBase64(string $data) : string {
        $base64 = base64_encode($data);

        if ($base64 === false) {
            return '';
        }
        $url = strtr($base64, '+/', '-_');
        return rtrim($url, '=');
    }

    private static function fromUrlBase64(string $data, bool $strict = false) : string {
        $data = strtr($data, '-_', '+/');
        return base64_decode($data, $strict);
    }
}
