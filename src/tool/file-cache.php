<?php

/**
 * Slouží pro uložení souborů do cache na disku.
 * ====================================================================================================================
 */
final class FileCache {

    private const SUFFIX_LENGTH = 10;

    private $cacheDir;

    public function __construct(string $cacheDir) {
        $this->cacheDir = self::initCacheDir($cacheDir);
    }

    public function getCachedFile(?string $fileGuid, ?int $checksum, ?string $extension, string $suffix = '') {
        if (!isset($fileGuid)) {
            return null;
        }
        $path = $this->buildCachedFilename($fileGuid, $checksum, $extension, $suffix);
        $files = glob($path.'*'); // pod tímto jménem by měl být maximálně 1 soubor
        return !empty($files) ? $files[0] : null;
    }
    
    public function cacheFileHandle(&$handle, string $fileGuid, int $checksum, string $extension, string $suffix = '') : void {
        // smaže všechny soubory, protože GUID je unikátní
        $files = glob($this->cacheDir.'/'.$fileGuid.'-*');

        if (!empty($files)) {
            foreach ($files as $file) {
                $fileName = basename($file);
                preg_match('/.+-(.+?)\\..+/', $fileName, $matches);
                $fileChecksum = $matches[1];

                if ($checksum != $fileChecksum) {
                    unlink($file);
                }
            }
        }
        // vytvoří cache soubor
        $cacheFilename = $this->buildCachedFilename($fileGuid, $checksum, $extension, $suffix);
        $cachedFileHandle = fopen($cacheFilename, 'w');
        stream_copy_to_stream($handle, $cachedFileHandle);
        fclose($cachedFileHandle);
        // nahradí příchozí handle za handle na soubor v cache
        fclose($handle);
        $handle = fopen($cacheFilename, 'r');
    }

    private function buildCachedFilenameBase(string $fileGuid, int $checksum) : string {
        return $this->cacheDir.'/'.$fileGuid.'-'.$checksum;
    }

    private function buildCachedFilename(string $fileGuid, int $checksum, string $extension, string $suffix) : string {
        $suffix = !empty($suffix) ? '.'.$suffix : '';
        return $this->buildCachedFilenameBase($fileGuid, $checksum).$suffix.'.'.$extension;
    }

    private static function initCacheDir($dir) {
        $current = glob($dir.'-*');

        if (!empty($current)) {
            $dir = $current[0];
        } else {
            $dir .= '-'.self::generateRandomSuffix();
        }
        // vytvoří cache adresář pokud neexistuje
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        return $dir;
    }

    private static function generateRandomSuffix() : string {
        $letters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $max = strlen($letters) - 1;
        $suffix = '';

        for ($i = 0; $i < self::SUFFIX_LENGTH; $i++) {
            $suffix .= $letters[rand(0, $max)];
        }
        return $suffix;
    }
}
