<?php

/**
 * Pomocné funkce.
 */
final class Utils {
    
    /**
     * Převede velikost v bytech na čitelný formát.
     */
    public static function formatSize(int $size) : string {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        for ($i = 0; $size > 1024; $i++) {
            $size /= 1024;
        }
        return round($size, 2).' '.$units[$i];
    }
    
    /**
     * Získá formátované datum včetně času.
     */
    public static function toDateTimeFormat(int $time) : string {
        return date('d.m.Y - H:i:s', $time / 1000);
    }
    
    /**
     * Získá formátované datum.
     */
    public static function toDateFormat(int $time) : string {
        return date('d.m.Y', $time / 1000);
    }
    
    /**
     * Převede čas v msec do formátovaného elapsed času.
     */
    public static function toElapsedTime(int $time) : string {
        if ($time == 0) {
            return 'now';
        }
        $time /= 1000; // odebereme msec
        $seconds = floor( $time % 60);
        $minutes = floor(($time / 60) % 60);
        $hours   = floor(($time / (60 * 60)) % 24);
        $days    = floor( $time / (60 * 60 * 24));
        $output  = '';
        
        if ($days > 0) {
            $output .= $days.'d ';
        }
        if ($hours > 0) {
            $output .= $hours.'h ';
        }
        if ($minutes > 0) {
            $output .= $minutes.'min ';
        }
        if ($seconds > 0) {
            $output .= $seconds.'s ';
        }
        return trim($output);
    }
    
    /**
     * Vygeneruje náhodný GUID.
     */
    public static function generateGuid() : string {
        return sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }
}
