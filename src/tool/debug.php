<?php

/**
 * Třída, která slouží pro vypsání objektu na stránku.
 * ====================================================================================================================
 */
final class Debug {

    private const ITEM_STYLE = '
        font: normal 13px consolas, monospace;
        padding: 5px;
        margin: 5px;
        position: relative;
        display: inline-block;
        background-color: rgba(34,34,34,0.8);
        border: 1px solid rgb(24,24,24);
        z-index: 9999;';

    public static function show($obj) {
        $f = function ($value = '', $padding = '&nbsp;&nbsp;&nbsp;&nbsp;', $paddingCount = -1, $key = null) use (&$f) {
            $out = '';

            if (is_object($value)) {
                // $out .= '<b style="color:#fa0">Type -> '.get_class($value).'</b>';
            }
            if (in_array(gettype($value), ['object', 'array'])) {
                $out .= ($paddingCount != -1) ? str_repeat($padding, $paddingCount) : '';

                if (!is_null($key)) {
                    $a_value = is_object($value) ? get_class($value) : gettype($value);
                    $out .= '<b style="color:#fa0">'.$key.'</b> <span style="color:#aaa">('.$a_value.')</span>';
                }
                $out .= '<br>';
                $counter = 0;

                foreach ($value as $k => $v) {
                    $out .= $f($v, $padding, $paddingCount + 1, $k);
                    $out .= '<br>';
                    $counter++;
                }
                if ($counter == 0) {
                    $out .= '<b style="color:#f66">NULL</b>';
                }
            } else {
                $out .= ($paddingCount != -1) ? str_repeat($padding, $paddingCount) : '';
                $out .= is_null($key) ? '' : '<span style="color:#eee">'.$key.':</span>&nbsp;';

                $style = function ($a_color, $a_value) use ($value, &$out) {
                    $out .= '<span style="color:'.$a_color.'" title="'.gettype($value).'">'.$a_value.'</span>';
                };
                if (is_null($value)) {
                    $style('#f66', 'NULL');
                } else if (is_numeric($value)) {
                    $style('#ff6', $value);
                } else if (is_bool($value)) {
                    $style('#ff6', $value ? 'true' : 'false');
                } else if (is_string($value) && strlen($value) == 0) {
                    $style('#f66', 'BLANK');
                } else if (is_string($value)) {
                    $style('#6f6', '\''.$value.'\'');
                } else {
                    $style('#6f6', $value);
                }
            }
            return $out;
        };
        $x =
            '<script type="text/javascript">function onClickDbg(thus) { thus.style.display = \'none\'; }</script>'.
            '<div class="dbg" ondblclick="onClickDbg(this)" style="'.self::ITEM_STYLE.'">'.$f($obj).'</div>';
        print_r($x);
    }
}
