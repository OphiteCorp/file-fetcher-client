<?php

require_once 'locale.cs.php';
require_once 'locale.sk.php';
require_once 'locale.en.php';

/**
 * Stará se o překlady kódů dle jazyka prohlížeče.
 */
final class Localizer {
    
    private const DEFAULT_LANGUAGE = 'en';
    
    private static $instance = null;
    
    private
        $currentLanguage,
        $map;
    
    /**
     * Vyhodnotí jazyk prohlížeče, určí výchozí a init objektu.
     */
    private function __construct() {
        // obsahuje všechny lokalizace
        $this->map = [
            self::DEFAULT_LANGUAGE => new En(),
            'cs'                   => new Cs(),
            'sk'                   => new Sk()
        ];
        $langCode = @substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        
        if (!isset($langCode)) {
            $langCode = self::DEFAULT_LANGUAGE;
        }
        $isSupported = $this->isLocaleSupported($langCode);
        $this->currentLanguage = $isSupported ? strtolower($langCode) : self::DEFAULT_LANGUAGE;
    }
    
    /**
     * Invoke se používá pro získání lokalizované zprávy podle klíče.
     */
    public function tr(string $key, ?array $params = []) : string {
        $messages = $this->map[$this->currentLanguage]->getMessages();
        $keyExists = array_key_exists($key, $messages);
        $msg = null;
        
        if ($keyExists) {
            $msg = $messages[$key];
            
            // přidá parametry
            if (!empty($params)) {
                for ($i = 0; $i < count($params); $i++) {
                    $msg = preg_replace('~{' . $i . '}~', $params[$i], $msg);
                }
            }
        } else {
            $msg = '??['.$key.']??';
        }
        return $msg;
    }
    
    /**
     * Získá aktuální jazyk prohlížeče.
     */
    public function getLangCode() : string {
        return $this->currentLanguage;
    }
    
    /**
     * Získá instanci třídy.
     */
    public static function instance() : Localizer {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    private function isLocaleSupported(string $langCode) : bool {
        return array_key_exists($langCode, $this->map);
    }
}
