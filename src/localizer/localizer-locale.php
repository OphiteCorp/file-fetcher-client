<?php

require_once dirname(__FILE__).'/localizer-codes.php';

/**
 * Stará se o překlady kódů dle jazyka prohlížeče.
 */
interface ILocalizerLocale {
    
    /**
     * Získá všechny lokalizované zprávy.
     */
    public function getMessages() : array;
}
