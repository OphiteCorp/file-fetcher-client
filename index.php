<?php

require_once dirname(__FILE__).'/src/gallery.php';

$gallery = new Gallery();
$localizer = $gallery->getLocalizer();
$header = $gallery->loadHeaderData();
$token = $gallery->getTokenData();

// pomocná funkce pro lokalizace
$tr = function(string $key, ?array $params = []) use (&$localizer) : string {
    return $localizer->tr($key, $params);
}

?>

<!DOCTYPE html>
<html lang="<?=$localizer->getLangCode()?>">
<head>
    <meta charset="utf-8">
    <meta name="author" content="<?=$header->author?>">
    <meta name="copyright" content="OphiteCorp">
    <meta name="description" content="<?=$header->description?>">
    <meta name="keywords" content="<?=$header->keywords?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="robots" content="noindex,nofollow">
    <meta property="og:title" content="<?=$header->ogTitle?>">
    <meta property="og:description" content="<?=$header->ogDescription?>">
    <meta property="og:image" content="<?=$header->ogImageUrl?>">

    <title><?=$header->title?></title>

    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="static/css/normalize.css">
    <link rel="stylesheet" href="static/css/common.css">

    <script src="static/js/jQuery/jquery.js" type="text/javascript"></script>
    <script src="static/js/lc_lightbox.lite.js" type="text/javascript"></script>

    <link rel="stylesheet" href="static/css/lc_lightbox.css">
    <link rel="stylesheet" href="static/skins/minimal.css">

    <script src="static/js/AlloyFinger/alloy_finger.min.js" type="text/javascript"></script>
    <script src="static/js/tooltip.js" type="text/javascript"></script>

    <script src="static/js/typed.js" type="text/javascript"></script>
    <script src="static/js/notify.js" type="text/javascript"></script>
    <script src="static/js/common.js" type="text/javascript"></script>

    <script type="text/javascript">
        Gallery.init({
            token:    '<?=Gallery::PARAM_TOKEN?>',
            file:     '<?=Gallery::PARAM_FILE_GUID?>',
            checksum: '<?=Gallery::PARAM_FILE_CHECKSUM?>'
        }, {
            remainingTime: <?=$token->remainingTime?>
        }, {
            fileLinkTitle:  '<?=$tr(LC::OPEN_LINK_TITLE)?>',
            openingFileNtf: '<?=$tr(LC::OPENING_PREVIEW)?>',
            by:             '<?=$tr(LC::BY)?>'
        })
    </script>
</head>
<body>
<div id="container">
    <?php
    if (!DEF_HIDE_LOGO) {
        echo '<div id="logo">';
        echo sprintf('<h1>%s</h1>', $header->title);
        echo sprintf('<h2 tt="%s">%s %s</h2>', $tr(LC::TITLE_AUTHOR_MSG), $tr(LC::BY), $header->author);
        echo '</div>';
    }
    // pomocné proměnné pro top a navigaci
    $qParams = $gallery->getUrlQueryParams([Gallery::PARAM_CATEGORY_GUID]);
    $pathsCount = count($gallery->getCatData()->paths);
    $isHiddenLogo = DEF_HIDE_LOGO ? 'hidden-logo' : '';
    ?>
    <div id="user-bar" class="<?=$isHiddenLogo?>">
        <div class="buttons">
            <?php
            if ($gallery->isServiceAvailable() && empty($token->login)) {
                echo sprintf('<a href="login.php">%s</a>', $tr(LC::LOG_IN));
            } else if (!empty($token->login)) {
                echo sprintf('<a href="change-password.php?%s=%s">%s</a><span class="spacer">|</span>', Gallery::PARAM_TOKEN, $token->nextToken, $tr(LC::CHANGE_PASSWORD));
                echo sprintf('<a href="logout.php">%s</a>', $tr(LC::LOGOUT));
            }
            ?>
        </div>
        <div class="current-user">
            <?php
            if (!empty($token->login)) {
                echo sprintf('<span class="access">%s <span class="login-as">%s. </span> %s: <span class="expired-time">00:00:00</span></span>', $tr(LC::LOGIN_AS), $token->login, $tr(LC::EXPIRES_IN));
            }
            ?>
        </div>
    </div>
    <div id="navigation" class="<?=(($pathsCount > 0) ? 'nav-show' : 'nav-hide')?>">
        <?php
        // vygeneruje navigaci / breadcrumb
        // ---------------------------------------------------------------------------------------
        for ($i = 0; $i < $pathsCount; $i++) {
            $path = $gallery->getCatData()->paths[$i]; // objekt CategoryPathDto
            $url = '?'.(($i == 0) ? $qParams : sprintf('%s=%s%s', Gallery::PARAM_CATEGORY_GUID, $path->guid, !empty($qParams) ? '&'.$qParams : ''));
            $lastOne = ($i == $pathsCount - 1);
            $tt = [];

            if (!DEF_HIDE_TOOLTIPS) {
                if (isset($path->description)) $tt[] = sprintf('<span class=\'tt-value\'>%s:</span> %s', $tr(LC::DESCRIPTION), $path->description);
            }
            $tt = implode('<br/>', $tt);
            $hrefTitle = sprintf('tt="%s"', ($i == 0) ? $tr(LC::HOME_TT) : $tt);
            echo sprintf('<a class="%s" href="%s" '.$hrefTitle.' content="%s" content-desc="%s">%s</a>', ($lastOne ? 'last' : ''), $url, $path->name, $path->description, ($lastOne ? '' : $path->name));

            if (!$lastOne) {
                echo '<span> &raquo; </span>';
            } else {
                echo sprintf('<script type="text/javascript">Gallery.typedOn(document.querySelector("#navigation a.last"), "%s");</script>', $path->name);
            }
        }
        ?>
    </div>
    <div id="navigation-footer">
        <?php
        // vygeneruje odkazy pro sdílení
        // ---------------------------------------------------------------------------------------
        if ($header->categoryGuid != null) {
            echo sprintf('<a class="fb-cat-share" href="javascript: void(0)" onclick="Gallery.openFacebookShare();">%s</a>', $tr(LC::LINK_SHARE_CATEGORY_ON_FB));
        } else {
            echo '&nbsp;';
        }
        ?>
    </div>

    <div id="content">
    <?php
    // vygeneruje kategorie (pokud nějaké jsou)
    // ---------------------------------------------------------------------------------------
    $categories = $gallery->getCatData()->categories;
    $printedCategories = 0;

    if (!empty($categories)) {
        echo '<div id="categories">';

        foreach ($categories as $cat) {
            if ((DEF_HIDE_EMPTY_CATEGORIES && $cat->isEmpty()) || (DEF_HIDE_CATEGORIES_WITHOUT_PERMISSION && $cat->protected && !$cat->accessAllowed)) {
                continue;
            }
            $printedCategories++;
            $subcategoriesClass = ($cat->subCategoriesCount > 0) ? 'class="active"' : '';
            $filesCountClass = ($cat->filesCount > 0) ? ' class="active" ' : '';
            $totalFilesCountClass = ($cat->totalFilesCount > 0) ? ' class="active" ' : '';
            $lockedDirClass = $cat->protected ? 'locked' : '';
            $url = Gallery::PARAM_CATEGORY_GUID.'='.$cat->guid.'&'.$qParams;
            $tt = [];

            if ($cat->protected && !$cat->accessAllowed) {
                $url = '#';
            }
            if (!DEF_HIDE_TOOLTIPS) {
                if (isset($cat->description)) $tt[] = sprintf('<span class=\'tt-value\'>%s:</span> %s', $tr(LC::DESCRIPTION), $cat->description);
                if (isset($cat->created)) $tt[] = sprintf('<span class=\'tt-value\'>%s:</span> %s', $tr(LC::CREATED), $cat->created);
                if (isset($cat->protected)) $tt[] = sprintf('<span class=\'tt-value\'>%s:</span> %s', $tr(LC::PROTECTED_BY_USER), ($cat->protected ? $tr(LC::YES) : $tr(LC::NO)));
                if (isset($cat->accessAllowed)) $tt[] = sprintf('<span class=\'tt-value\'>%s:</span> %s', $tr(LC::ACCESS_ALLOWED), ($cat->accessAllowed ? $tr(LC::YES) : $tr(LC::NO)));
            }
            $tt = implode('<br/>', $tt);
            $pid = $gallery->generateGuid(); // preview ID (unikátní ID pro link kategorie)

            echo sprintf('<a data-tilt class="elem-dir %s" href="?%s">', $lockedDirClass, $url);

            if (!DEF_HIDE_TAG_NEW && $cat->newlyAdded) {
                echo sprintf('<span class="new %s" tt="<span class=\'tt-new\'>%s</span><br/>%s: <span class=\'tt-value\'>%s</span>"></span>', (DEF_HIDE_CATEGORY_HEAD ? 'new-no-head' : ''), $tr(LC::NEW_OBJ), $tr(LC::NEW_OBJ_REMOVED_IN), $cat->newlyAddedLeft);
            }
            if (!DEF_HIDE_CATEGORY_HEAD) {
                echo sprintf('<div class="head"><span tt="%s">%s</span>', $tr(LC::CATEGORY_DATE_TT), $cat->created);
                if ($cat->accessAllowed) {
                    echo '<span class="detail">';
                    echo sprintf('<span %s tt="%s">%s</span>|', $filesCountClass, $tr(LC::CATEGORY_FILES_TT), $cat->filesCount);
                    echo sprintf('<span %s tt="%s">%s</span>|', $subcategoriesClass, $tr(LC::SUBCATEGORIES_IN_CATEGORY_TT), $cat->subCategoriesCount);
                    echo sprintf('<span %s tt="%s <span class=\'tt-value\'>%s</span>">%s</span>', $totalFilesCountClass, $tr(LC::NUMBER_FILES_WITH_SIZE_TT), $cat->totalCategorySizeReadable, $cat->totalFilesCount);
                    echo '</span>';
                }
                echo '</div>';
            }
            // zobrazí vzdálený náhled kategorie
            if ($cat->previewFile != null && !DEF_FORCE_STATIC_DIR_PREVIEW) {
                $previewFile = $gallery->getFilePreviewUrl($cat->previewFile->guid, $cat->previewFile->checksum, $cat->previewFile->width, $cat->previewFile->height);
                echo sprintf('<div class="dir dir-preview pid-%s" tt="%s" lazy-file="%s"></div>', $pid, $tt, $previewFile);
                echo sprintf('<script type="text/javascript">Gallery.loadCategoryPreview(".pid-%s")</script>', $pid);
            } else {
                echo sprintf('<div class="dir dir-static" tt="%s"></div>', $tt);
            }
            echo sprintf('<div class="title title-%s"></div>', $pid);
            echo sprintf('<script type="text/javascript">Gallery.typedOn(".title-%s", "%s");</script>', $pid, $cat->name);
            echo '</a>';
        }
        echo '</div>';
    }
    // zobrazí oddělovač nad obsahem (soubory/obrázky)
    // ---------------------------------------------------------------------------------------
    $files = $gallery->getCatData()->files;
    $printFilesLine = (!empty($categories) && !empty($files));
    $printedFiles = 0;

    if ($printFilesLine) {
        echo sprintf('<div id="files-line">%s</div>', $tr(LC::CONTENT));
    }
    // vygeneruje soubory pro aktuální kategorii (pokud nějaké jsou)
    // ---------------------------------------------------------------------------------------
    if (!empty($files)) {
        echo '<div id="files">';

        foreach ($files as $file) {
            if (DEF_HIDE_NON_EXISTS_FILES && !$file->exists) {
                continue;
            }
            $printedFiles++;
            $originUrl = $gallery->getFilePreviewUrl($file->guid, $file->checksum, $file->imgWidth, $file->imgHeight, FileFetcherApiWrapperResolver::PM_LARGE);
            $downloadUrl = $gallery->getFileOpenUrl($file->guid, $file->checksum);
            $previewUrl = $gallery->getFilePreviewUrl($file->guid, $file->checksum, $file->imgWidth, $file->imgHeight);
            $pid = $gallery->generateGuid(); // preview ID
            $title = isset($file->name) ? $file->name : '';
            $descElem = isset($file->description) ? sprintf('data-lcl-txt="%s"', $file->description) : '';
            $authorElem = isset($file->author) ? sprintf('data-lcl-author="%s"', $file->author) : '';
            $existsClass = !$file->exists ? 'file-not-exists' : '';
            $tt = [];

            if ($file->extraType == FileResponseDto::ET_VIDEO) {
                $originUrl = $gallery->getFileOpenUrl($file->guid, $file->checksum);
            }
            if (!DEF_HIDE_TOOLTIPS) {
                if (isset($file->name)) $tt[] = '<span class=\'tt-value\'>'.$tr(LC::NAME).':</span> '.$file->name;
                if (isset($file->description)) $tt[] = '<span class=\'tt-value\'>'.$tr(LC::DESCRIPTION).':</span> '.$file->description;
                if (isset($file->createdRecord)) $tt[] = '<span class=\'tt-value\'>'.$tr(LC::CREATED).':</span> '.$file->createdRecord;
                if (isset($file->createdFile)) $tt[] = '<span class=\'tt-value\'>'.$tr(LC::FILE_TAKEN_IN).':</span> '.$file->createdFile;
                if (isset($file->size)) $tt[] = '<span class=\'tt-value\'>'.$tr(LC::FILE_SIZE).':</span> '.$file->sizeReadable;
                if (isset($file->extension)) $tt[] = '<span class=\'tt-value\'>'.$tr(LC::FILE_EXTENSION).':</span> '.$file->extension;
                if (isset($file->grade)) $tt[] = '<span class=\'tt-value\'>'.$tr(LC::GRADE).':</span> '.$file->grade;
                if (isset($file->author)) $tt[] = '<span class=\'tt-value\'>'.$tr(LC::AUTHOR).':</span> '.$file->author;

                if ($file->extraType == FileResponseDto::ET_IMAGE || $file->extraType == FileResponseDto::ET_VIDEO) {
                    if (isset($file->imgWidth) && isset($file->imgHeight)) $tt[] = sprintf('<span class=\'tt-value\'>%s:</span> %sx%s', $tr(LC::IMAGE_DIMENSION), $file->imgWidth, $file->imgHeight);
                    if (isset($file->videoDuration)) $tt[] = sprintf('<span class=\'tt-value\'>%s:</span> %s', $tr(LC::VIDEO_DURATION), Utils::toElapsedTime($file->videoDuration * 1000));
                }
            }
            $tt = implode('<br/>', $tt);
            $pid = $gallery->generateGuid(); // preview ID (unikátní ID pro link souboru)
            $typeAttr = sprintf('data-lcl-type="%s"', $file->exists ? $file->extraType : FileResponseDto::ET_IMAGE);

            // s tím title je tady hack, protože lc_lightbox vyžaduje title pro název, takže to není kompatabilní s tooltipem,
            // proto se po najetí myši musí smazat title, aby fungoval tooltip a ve všech ostatních případech opět zobrazit
            echo '<a data-tilt class="elem '.$existsClass.'" '.$typeAttr.' href="'.($file->exists ? $originUrl : $previewUrl).'" title="'.$title.'" '.$descElem.' '.$authorElem.' data-lcl-thumb="'.$previewUrl.'" data-lcl-down="'.($file->exists ? $downloadUrl : '').'" onmouseover="this.title = \'\'" onmouseout="this.title = \''.$title.'\'" onclick="this.title = \''.$title.'\'">';

            if (!DEF_HIDE_TAG_NEW && $file->newlyAdded) {
                echo sprintf('<span class="new" tt="<span class=\'tt-new\'>%s</span><br/>%s: <span class=\'tt-value\'>%s</span>"></span>', $tr(LC::NEW_OBJ), $tr(LC::NEW_OBJ_REMOVED_IN), $file->newlyAddedLeft);
            }
            if ($file->extraType == FileResponseDto::ET_VIDEO) {
                echo sprintf('<span class="video-flag" tt="%s"></span>', $tr(LC::VIDEO));
            }
            echo sprintf('<div class="thumb pid-%s" tt="%s" lazy-file="%s"></div>', $pid, $tt, $previewUrl);
            echo sprintf('<script type="text/javascript">Gallery.loadFilePreview(".pid-%s")</script>', $pid);

            if (!DEF_HIDE_TAG_GRADE && isset($file->grade)) {
                echo sprintf('<span class="grade" tt="<strong>%s</strong><br/>%s">%s</span>', $tr(LC::GRADE_TITLE_TT), $tr(LC::GRADE_DESC_TT), $file->grade);
            }
            if (!DEF_HIDE_TAG_FILE_LINK && $file->exists) {
                echo sprintf('<span class="open-link" onclick="Gallery.openLink(event)" tt="%s">•</span>', $tr(LC::OPEN_LINK_TT));
            }
            if (isset($file->name)) {
                $staticTitleClass = DEF_ALWAYS_DISPLAY_TITLE ? 'title-static' : '';
                echo sprintf('<div class="title title-%s %s"></div>', $pid, $staticTitleClass);
                echo sprintf('<script type="text/javascript">Gallery.typedOn(".title-%s", "%s");</script>', $pid, $file->name);
            }
            echo '</a>';
        }
        if ($printedFiles == 0) {
            echo sprintf('<div class="empty">%s</div>', $tr(LC::MSG_NO_FILES));
        }
        echo '</div>';
    }
    // zobrazí informaci, že neexistují žádné soubory
    // ---------------------------------------------------------------------------------------
    if ($gallery->isEmpty() || ($printedFiles == 0 && $printedCategories == 0)) {
        echo '<div class="empty">';
        echo '<p>'.$tr(LC::MSG_NO_CONTENT).'</p>';
        echo '<p class="ascii">'.$tr(LC::MSG_NO_CONTENT_ASCII).'</p>';
        echo '</div>';
    }
    ?>
    </div>

    <?php
    if (!DEF_HIDE_FOOTER) {
        echo '<div id="footer">';
        $stats = $gallery->getStatsData();
        $tts = [];
        $tt = [];

        // tooltip s uptime služby
        if ($gallery->isServiceAvailable() && $stats != null) {
            $tt[] = sprintf('%s: <span class=\'tt-value\'>%s</span>', $tr(LC::FOOTER_UPTIME), $stats->uptime);
        }
        $tts[] = implode('<br/>', $tt);
        $tt = [];

        // tooltip s verzi galerie
        if ($gallery->isServiceAvailable() && $stats != null) {
            $tt[] = sprintf('%s: <span class=\'tt-value\'>%s</span>', $tr(LC::FOOTER_NUM_ALL_CATEGORIES), $stats->totalCategories);
            $tt[] = sprintf('%s: <span class=\'tt-value\'>%s</span>', $tr(LC::FOOTER_NUM_ALL_FILES), $stats->totalFiles);
            $tt[] = sprintf('&nbsp;-&nbsp;%s: <span class=\'tt-value\'>%s</span>', $tr(LC::FOOTER_NUM_ALL_IMAGES), $stats->totalImages);
            $tt[] = sprintf('&nbsp;-&nbsp;%s: <span class=\'tt-value\'>%s</span>', $tr(LC::FOOTER_NUM_ALL_VIDEOS), $stats->totalVideos);
            $tt[] = sprintf('%s: <span class=\'tt-value\'>%s</span>', $tr(LC::FOOTER_SIZE_ALL_FILES), $stats->totalFilesSizeReadable);
        }
        $tts[] = implode('<br/>', $tt);
        $versionCssClass = $gallery->isServiceAvailable() ? 'online' : 'offline';
        echo sprintf('<p class="copy">%s %s <span class="copy-author" tt="%s">%s</span> © %s</p>', $tr(LC::DESIGN_AND_CODE), $tr(LC::BY), $tr(LC::FOOTER_AUTHOR_CONTACT), $header->author, $gallery->getYearRange());
        echo sprintf('<p>%s: <span class="version-%s" tt="%s">%s</span>; %s: <span class="version-gallery" tt="%s">v%s</span></p>', $tr(LC::FILE_FETCHER_SERVICE), $versionCssClass, $tts[0], $gallery->getServerVersion(), $tr(LC::GALLERY), $tts[1], DEF_GALLERY_VERSION);
        echo sprintf('<p>%s %s</p>', $gallery->calculateLoadTime(), $tr(LC::SECONDS));

        if ($gallery->isServiceAvailable()) {
            $tt = [];
            $tt[] = sprintf('<strong>%s:</strong>', $tr(LC::PARAMETERS));
            $tt[] = sprintf('<span class=\'tt-value\'>?%s</span> %s <span class=\'tt-value\'>?%s=5</span> - %s', Gallery::PARAM_RANDOM, $tr(LC::WORD_OR), Gallery::PARAM_RANDOM, $tr(LC::HELP_RANDOM));
            $tt = implode('<br/>', $tt);
            echo sprintf('<p><span class="help-icon" tt="%s"></span></p>', $tt);
        }
        echo '</div>';
    }
    ?>
</div>

<script src="static/js/jQuery/tilt.jquery.js" type="text/javascript"></script>
<script type="text/javascript">Gallery.pageReady()</script>

</body>
</html>
