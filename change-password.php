<?php

require_once dirname(__FILE__).'/src/gallery.php';

$gallery = new Gallery();
$token = $gallery->getTokenData();
$localizer = $gallery->getLocalizer();
$errorMessage = '';

// uživatel není přihlášen
if (empty($token->login)) {
    header('Location: /');
    exit;
}
// uživatel klikne na tlačítko pro přihlášení
if (isset($_POST['submit']) && !empty($_POST['new-password'])) {
    $login = $token->login;
    $password = $token->password;
    $newPassword = $_POST['new-password'];

    if ($gallery->changePassword($login, $password, $newPassword)) {
        header('Location: ' . sprintf('/?%s=%s', Gallery::PARAM_TOKEN, $gallery->getTokenData()->nextToken));
        exit;
    } else {
        $errorMessage = $localizer->tr(LC::ERROR_CHANGE_PASSWORD);
    }
}

$header = $gallery->loadHeaderData();

?>

<!DOCTYPE html>
<html lang="<?=$localizer->getLangCode()?>">
    <head>
        <meta charset="utf-8">
        <meta name="author" content="<?=$header->author?>">
        <meta name="copyright" content="OphiteCorp">
        <meta name="description" content="<?=$header->description?>">
        <meta name="keywords" content="<?=$header->keywords?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="robots" content="noindex,nofollow">

		<title><?=$localizer->tr(LC::PAGE_CHANGE_PASSWORD_TITLE)?></title>
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="static/css/normalize.css">
        <link rel="stylesheet" href="static/css/common.css">
        <link rel="stylesheet" href="static/css/login.css">
        
        <script src="static/js/jQuery/jquery.js" type="text/javascript"></script>
        <script src="static/js/common.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            const textChangeEvent = () => {
                const newPassword = $('input[name="new-password"]').val()
                const submit = $('input[type="submit"]')

                if (newPassword.length > 0) {
                    $(submit).removeClass('submit-off')
                } else {
                    $(submit).addClass('submit-off')
                }
            }
            $(document).ready(() => {
                $('input[name="new-password"]').on('keyup', e => textChangeEvent(e))
            })
        </script>
    </head>
    
    <body>
        <div id="container">
            <div id="login-form">
                <h1><?=$localizer->tr(LC::PAGE_CHANGE_PASSWORD_HEADER)?></h1>
                
                <form method="post">
                    <div class="field">
                        <input type="password" name="new-password" placeholder="<?=$localizer->tr(LC::NEW_PASSWORD)?>">
                    </div>
                    <div class="field">
                        <input type="submit" name="submit" class="submit-off" value="<?=$localizer->tr(LC::CHANGE)?>">
                    </div>
                    <?=sprintf('<a href="/?%s=%s">%s</a>', Gallery::PARAM_TOKEN, $token->nextToken, $localizer->tr(LC::BACK))?>
                </form>
                <div class="err-msg"><?=$errorMessage?></div>
            </div>
        </div>
    </body>
</html>
