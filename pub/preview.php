<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/src/api/server-api-wrapper-resolver.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/src/tool/file-cache.php';

$resolver = new FileFetcherApiWrapperResolver();
$callback = function (object $data) use (&$resolver) : bool {
    $cache = new FileCache(DEF_CACHE_DIR);
    $cacheFileSuffix = 'thumb'; // výchozí (malý náhled do galerie)

    switch ($data->mode) {
        case FileFetcherApiWrapperResolver::PM_LARGE:
            $cacheFileSuffix = 'large'; // velký náhled
            break;
        case FileFetcherApiWrapperResolver::PM_ORIGIN:
            $cacheFileSuffix = ''; // původní kvalita
            break;
    }
    // ještě neproběhlo volání serveru
    // v tomto kroku zjistíme existenci náhledu souboru v cache, aby nebylo nutné volat server
    if (!isset($data->handle)) {
        // checksum musí být číslo, pokud nebude, tak nastavíme 0 (nikdy to nenajde v cache)
        if (!is_numeric($data->fileChecksum)) {
            $data->fileChecksum = 0;
        }
        $cachedFile = $cache->getCachedFile($data->fileGuid, $data->fileChecksum, $data->fileExtension, $cacheFileSuffix);

        // v cache existuje náhled souboru - otevřeme ho
        if ($cachedFile != null) {
            $fileAccess = $resolver->execute(FileFetcherApiWrapperResolver::TYPE_FILE_ACCESS, [
                'fileGuid' => $data->fileGuid
            ]);
            // náhled v cache existuje a je potřeba zkontrolovat i oprávnění uživatele, protože
            // soubor může být pod heslem a bez této podmínky by byl dříve vytvořený náhled veřejný
            if ($fileAccess != null && $fileAccess->available) {
                $data->handle = fopen($cachedFile, 'r');
            } else {
                $data->handle = fopen(DEF_FAILBACK_PREVIEW_IMAGE, 'r');
            }
            return false;
        }
    }
    // server byl zavolán
    else {
        $error = $data->fileError;

        if (!$error) {
            $fileInfo = $resolver->execute(FileFetcherApiWrapperResolver::TYPE_FILE_INFO, [
                'fileGuid' => $data->fileGuid
            ]);
            if ($fileInfo != null) {
                $cache->cacheFileHandle($data->handle, $data->fileGuid, $data->fileChecksum, $data->fileExtension, $cacheFileSuffix);
            } else {
                $error = true; // soubor s GUID na serveru neexistuje
            }
        }
        if ($error) {
            if ($data->handle) {
                fclose($data->handle);
            }
            $data->handle = fopen(DEF_FAILBACK_PREVIEW_IMAGE, 'r');
            return false;
        }
    }
	return true;
};

$resolver->execute(FileFetcherApiWrapperResolver::TYPE_FILE_IMAGE, [], $callback);
