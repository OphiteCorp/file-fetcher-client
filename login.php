<?php

require_once dirname(__FILE__).'/src/gallery.php';

$gallery = new Gallery();
$token = $gallery->getTokenData();
$changePassword = isset($_GET['change']);

// uživatel je již přihlášen
if (!empty($token->login)) {
    header('Location: '.sprintf('/?%s=%s', Gallery::PARAM_TOKEN, $token->nextToken));
    exit;
}
// uživatel klikne na tlačítko pro přihlášení
if (isset($_POST['submit']) && !empty($_POST['login']) && !empty($_POST['password'])) {
    $login = $_POST['login'];
    $password = $_POST['password'];
    $gallery->login($login, $password);
    header('Location: '.sprintf('/?%s=%s', Gallery::PARAM_TOKEN, $gallery->getTokenData()->nextToken));
    exit;
}

$localizer = $gallery->getLocalizer();
$header = $gallery->loadHeaderData();

?>

<!DOCTYPE html>
<html lang="<?=$localizer->getLangCode()?>">
    <head>
        <meta charset="utf-8">
        <meta name="author" content="<?=$header->author?>">
        <meta name="copyright" content="OphiteCorp">
        <meta name="description" content="<?=$header->description?>">
        <meta name="keywords" content="<?=$header->keywords?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="robots" content="noindex,nofollow">

		<title><?=$localizer->tr(LC::PAGE_LOGIN_TITLE)?></title>
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="static/css/normalize.css">
        <link rel="stylesheet" href="static/css/common.css">
        <link rel="stylesheet" href="static/css/login.css">
        
        <script src="static/js/jQuery/jquery.js" type="text/javascript"></script>
        <script src="static/js/common.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            const textChangeEvent = (e, type) => {
                const password = $('input[name="password"]').val()
                const submit = $('input[type="submit"]')
                const loginElem = $('input[name="login"]')
                let login = loginElem.val().trim()

                if (type === 'login') {
                    login = login.replace(/[^A-Za-z0-9]/gi, '')
                    loginElem.val(login)
                }
                if (login.length > 0 && password.length > 0) {
                    $(submit).removeClass('submit-off')
                } else {
                    $(submit).addClass('submit-off')
                }
            }
            $(document).ready(() => {
                $('input[name="login"]').on('keyup', e => textChangeEvent(e, 'login'))
                $('input[name="password"]').on('keyup', e => textChangeEvent(e, 'password'))
            })
        </script>
    </head>
    
    <body>
        <div id="container">
            <div id="login-form">
                <h1><?=$localizer->tr(LC::PAGE_LOGIN_HEADER)?></h1>
                
                <form method="post">
                    <div class="field">
                        <input type="text" name="login" placeholder="<?=$localizer->tr(LC::USER)?>">
                    </div>
                    <div class="field">
                        <input type="password" name="password" placeholder="<?=$localizer->tr(LC::PASSWORD)?>">
                    </div>
                    <div class="field">
                        <input type="submit" name="submit" class="submit-off" value="<?=$localizer->tr(LC::LOG_IN)?>">
                    </div>
                    <a href="/"><?=$localizer->tr(LC::BACK)?></a>
                </form>
            </div>
        </div>
    </body>
</html>
