<?php

// ZÁKLADNÍ
// ======================================================================================

// verze galerie
define('DEF_GALLERY_VERSION', '1.1.0');

// rok vytvoření aplikace pro footer (neměnit)
define('DEF_YEAR_OF_CREATION', 2020);

// zapne nebo vypne vývojový režim
define('DEF_DEBUG_MODE', false);

// adresa API serveru, pro získání informací o galerií a obsahu
define('DEF_ENDPOINT', 'http://127.0.0.1:8080/api');

// ZABEZPEČENÍ
// ======================================================================================

// šablona tokenu, která slouží pro encode identity uživatele
// POZOR - šablona musí být stejná jako na serveru (používá se pro decode)
define('DEF_TOKEN_PATTERN', [
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', // origin
    'Ev63T7wi0Rfxo294lQUuYWqzasKk5MGZgCeOBbXjIFHVtcd1P8LDynmNrAShpJ'  // mixed
]);

// algoritmus pro hash hesla
// POZOR - pokud se změní, tak všechny existující účty přestanou fungovat!
define('DEF_PASSWORD_HASH_ALG', 'sha3-256');

// tajný klíč pro encrypt a decrypt dat (např. přihlašovacích údajů v URL)
define('DEF_SECRET_KEY', '926ad3107faf826d5e9f1148e0625544');

// délka platnosti tajného klíče v sekundách; pokud bude 0, tak nemá expiraci
define('DEF_SECRET_KEY_TTL', 30 * 60); // 30min

// pokud bude True, tak při každé obnově stránky se obnoví čas expirace tokenu
// funguje pouze v případě, že bude platný, pokud vyprší, tak není co obnovit
define('DEF_REFRESH_EXPIRE_TIME', false);

// OSTATNÍ
// ======================================================================================

// velikost náhledů souborů v galerií
// POZOR - pokud se změní, tak všechny náhledy v cache zůstanou a je nutné je ručně promazat a vygenerovat znovu
define('DEF_PREVIEW_SIZE', 600);

// velikost náhledů souborů v galerií po zvětšení (kliknutí na položku)
// POZOR - pokud se změní, tak všechny náhledy v cache zůstanou a je nutné je ručně promazat a vygenerovat znovu
define('DEF_LARGE_PREVIEW_SIZE', 1600);

// lokální adresář pro cache náhledů souborů
define('DEF_CACHE_DIR', dirname(__FILE__).'/.cache');

// výchozí typ (přípona) souboru náhledu (stejná hodnota je nastavena na serveru)
define('DEF_FAILBACK_PREVIEW_EXTENSION', 'jpg');

// výchozí náhled souboru, pokud všechno selže (nevztahuje se na náhledy kategorii)
define('DEF_FAILBACK_PREVIEW_IMAGE', $_SERVER['DOCUMENT_ROOT'].'/static/img/not-found.jpg');

// VIDITELNOST
// ======================================================================================

// pokud bude True, tak skryje logo
define('DEF_HIDE_LOGO', false);

// pokud bude True, tak skryje tooltipy pro každou kategorii, soubor a detailní informace o kategorii v navigaci
// nevztahuje se na základní tooltipy
define('DEF_HIDE_TOOLTIPS', false);

// pokud bude True, tak skryje celou patičku stránky včetně nápovědy
define('DEF_HIDE_FOOTER', false);

// ---

// pokud bude True, tak skryje prázdné adresáře (úplně prázdné včetně podadresářů)
define('DEF_HIDE_EMPTY_CATEGORIES', false);

// pokud bude True, tak skryje hlavičku u každé kategorie (datum a info)
define('DEF_HIDE_CATEGORY_HEAD', false);

// pokud bude True, tak skryje kategorie, do kterých uživatel nemá oprávnění
// POZOR - nemá vliv na počty souborů a adresářů
define('DEF_HIDE_CATEGORIES_WITHOUT_PERMISSION', true);

// pokud bude True, tak skryje značku NEW u kategorii a souborů
define('DEF_HIDE_TAG_NEW', false);

// pokud bude True, tak všechny adresáře budou mít obrázek složky (náhledy souborů nebudou fungovat)
define('DEF_FORCE_STATIC_DIR_PREVIEW', false);

// pokud bude True, tak skryje soubory, které neexistují
// POZOR - nemá vliv na počty souborů
define('DEF_HIDE_NON_EXISTS_FILES', false);

// pokud bude True, tak skryje značku GRADE u souborů
define('DEF_HIDE_TAG_GRADE', false);

// pokud bude True, tak skryje značku pro zobrazení přímého odkazu na soubor
define('DEF_HIDE_TAG_FILE_LINK', false);

// pokud bude True, tak vždy zobrazí titulek u souborů (jinak bude po najetí myši)
define('DEF_ALWAYS_DISPLAY_TITLE', true);
