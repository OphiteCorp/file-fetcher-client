const Gallery = {
    _params: {
        token:    null,
        file:     null,
        checksum: null,
        detail:   'o'
    },
    _opts: {
        remainingTime: null
    },
    _msgs: {
        fileLinkTitle:  null,
        openingFileNtf: null,
        by:             null
    },

    init: function (params, opts, msgs) {
        this._params = {...this._params, ...params}
        this._opts = {...this._opts, ...opts}
        this._msgs = {...this._msgs, ...msgs}
    },
    loadFilePreview: function (pidClass) {
        let time = setTimeout(() => {
            const elem = $(pidClass)
            const attrValue = elem.attr('lazy-file')
            elem.removeAttr('lazy-file')
            elem.css('background-image', 'url("' + attrValue + '")')

            const closest = elem.closest('.elem')
            $(closest).attr('data-lcl-thumb', attrValue)

            this.handleLoadImage(attrValue, () => {
                elem.closest('a').css('background', 'unset')
            })
            clearInterval(time)
        }, 0)
    },
    loadCategoryPreview: function (pidClass) {
        let time = setTimeout(() => {
            const elem = $(pidClass)
            const attrValue = elem.attr('lazy-file')
            elem.removeAttr('lazy-file')
            elem.css('background-image', 'url("' + attrValue + '")')

            this.handleLoadImage(attrValue, () => {
                elem.closest('a').css('background', 'unset')
            })
            clearInterval(time)
        }, 0)
    },
    openFacebookShare: function () {
        const urlParams = new URLSearchParams(window.location.search)
        const paramValue = urlParams.get(this._params.token)
        let target = window.location.href + ''

        if (paramValue) {
            const url = new URL(target)
            url.searchParams.delete(this._params.token)
            target = url
        }
        window.open('https://www.facebook.com/sharer?u=' + target + '&display=popup', 'sharer', 'toolbar=0,status=0,width=590,height=325')
    },
    typedOn: function (elem, value) {
        new Typed(elem, {
            strings: [value],
            typeSpeed: 24
        })
    },
    prepareNotifyStyles: function() {
        const style = {
            "padding": "8px 15px 8px 14px",
            "text-shadow": "0 1px 0 #000",
            "background-color": "rgba(0,0,0,0.8)",
            "border": "1px solid #2c2c2c",
            "font": "normal 15px 'Exo 2', Verdana",
            "border-radius": "6px",
            "white-space": "normal",
            "word-break": "break-all",
            "background-repeat": "no-repeat",
            "background-position": "3px 7px",
            "color": "#ccc",
            "line-height": "1.8em"
        }
        $.notify.addStyle('file_link', {
            html: "<div>\n<span class='ntf-title'>" + this._msgs.fileLinkTitle + "</span><br/><span data-notify-text></span>\n</div>",
            classes: {
                base: style
            }
        })
        $.notify.addStyle('message', {
            html: "<div>\n<span class='ntf-title' data-notify-text></span>\n</div>",
            classes: {
                base: style
            }
        })
    },
    notify: function(msg, style, timeout) {
        $.notify(msg, {
            clickToHide: false,
            autoHide: true,
            style: style,
            autoHideDelay: timeout || 5000
        })
    },
    resolveAutoOpen: function() {
        const urlParams = new URLSearchParams(window.location.search)
        const file = urlParams.get(this._params.detail)

        if (file && file.length > 0) {
            let opened = false

            $('a.elem').each((k, v) => {
                const href = $(v).attr('href')
                const url = new URL('http://' + href)
                const fileGuid = url.searchParams.get(this._params.file)

                if (fileGuid === file) {
                    opened = true

                    const delay = 1000
                    this.notify(this._msgs.openingFileNtf, 'message', delay)
                    let time = setTimeout(() => {
                        v.click()
                        clearTimeout(time)
                    }, delay)
                    return false
                }
            })
            if (!opened) {
                this.removeParamFromUrl(this._params.detail, true)
            }
        }
    },
    openLink: function(e) {
        this.disableClickEvents(e)
        const fileUrl = $(e.target).closest('a').attr('href')
        const file = fileUrl.match(/.+\?.=(\w+)/)[1]
        const url = new URL(window.location.href + '')
        url.searchParams.set(this._params.detail, file)
        this.notify(url, 'file_link')
        return false
    },
    removeParamFromUrl: function(paramName, redirect) {
        const urlParams = new URLSearchParams(window.location.search)
        const paramValue = urlParams.get(paramName)
        let newUrl = null

        if (paramValue) {
            const url = new URL(window.location.href + '')
            url.searchParams.delete(paramName)
            history.replaceState(null, null, url + '')
            newUrl = url

            if (redirect) {
                window.location.href = url
            }
        }
        return newUrl
    },
    handleLoadImage: function (url, callback) {
        let img = new Image()
        img.onload = callback
        img.src = url

        if (img.complete) {
            img.onload(null)
        }
    },
    disableClickEvents: function(e) {
        e.stopPropagation() // lightbox
        e.preventDefault()  // a href
    },
    accessTokenTimer: function() {
        const loginAsElem = $('.current-user .login-as')

        if (loginAsElem.text().length === 0) {
            return
        }
        const expiredTimeElem = $('.current-user .expired-time')
        let timer = this._opts.remainingTime

        let interval = setInterval(() => {
            let hours   = Math.floor(timer / 3600)
            let minutes = Math.floor((timer - (hours * 3600)) / 60)
            let seconds = timer - (hours * 3600) - (minutes * 60)

            hours   = (hours < 10) ? '0' + hours : hours
            minutes = (minutes < 10) ? '0' + minutes : minutes
            seconds = (seconds < 10) ? '0' + seconds : seconds

            $(expiredTimeElem).text(hours + ':' + minutes + ':' + seconds)

            if (--timer < 0) {
                timer = 0
                window.location = '/'
                clearInterval(interval)
            }
        }, 1000)
    },
    pageReady: function() {
        const gallery = this

        $(document).ready(function() {
            Gallery.prepareNotifyStyles()

            lc_lightbox('.elem', {
                wrap_class: 'lcl_fade_oc',
                thumb_attr: 'data-lcl-thumb',
                down_attr: 'data-lcl-down',
                gallery: true,
                slideshow: true,
                slideshow_time: 2000,
                fullscreen: true,
                counter: true,
                show_title: true,
                show_descr: true,
                show_author: true,
                thumbs_nav: true,
                socials: true,
                download: true,
                modal: false,
                rclick_prevent: true,
                fs_img_behavior: 'fit',
                skin: 'minimal',
                max_width: '95%',
                max_height: '95%',
                ol_color: '#000',
                radius: 0,
                padding: 0,
                border_w: 1,
                border_col: '#000',
                thumbs_w: 110,
                thumbs_h: 110,
                open_close_time: 200,
                preload: false,
                on_close: () => {
                    gallery.removeParamFromUrl(gallery._params.detail, true)
                },
                msgs: {
                    by: gallery._msgs.by
                }
            })
            gallery.accessTokenTimer()
            tooltip.init()

            $('#footer p').each((i, elem) => {
                $(elem).slideDown()
            })
            $('span.new').on('click', e => {
                gallery.disableClickEvents(e)
                return false
            })
            $('span.grade').on('click', e => {
                gallery.disableClickEvents(e)
                return false
            })
            $('.elem-dir .head').on('click', e => {
                gallery.disableClickEvents(e)
                return false
            })
            gallery.resolveAutoOpen()
        })
        $('#logo').tilt({
            maxTilt:     10,
            perspective: 1000,
            disableAxis: 'y'
        })
        $('#categories a').tilt({
            maxTilt:     10,
            perspective: 650,
            glare: false
        })
        $('#files a').tilt({
            maxTilt:     10,
            perspective: 650,
            glare: false
        })
    }
}
